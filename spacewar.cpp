// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Draw animated spaceships with collision and shield
// Chapter 6 spacewar.cpp v1.0
// This class is the core of the game

#include "spaceWar.h"

//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{
}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError

    // background texture
    if (!tileTexture.initialize(graphics,BACKGROUND_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing background texture"));

	if (!heartTexture.initialize(graphics, HEART_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing heart textures"));

    // main game textures
    if (!gameTextures.initialize(graphics,TEXTURES_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

	if (!playerTextures.initialize(graphics, PLAYER_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player textures"));

	if (!shadowTextures.initialize(graphics, SHADOW_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing shadow textures"));

	if (!shadowStanding.initialize(graphics, SHADOW_STANDING))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing shadow textures"));

	if (!slashTexture.initialize(graphics, SLASH_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing slash textures"));

	

	//enemy texture
	if (!enemyTexture.initialize(graphics, ENEMY_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing enemy textures"));

	//easy enemy texture
	if (!easyenemyTexture.initialize(graphics, "pictures\\easyenemy.png"))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing mob texture"));

	//easy enemy image
	if (!easyenemy.initialize(this, 48, 48, 3, &easyenemyTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing mob"));
	easyenemy.setFrames(easyenemyNS::EasyEnemy_START_FRAME, easyenemyNS::EasyEnemy_END_FRAME);
	easyenemy.setFrameDelay(easyenemyNS::ANIMATION_DELAY);

	//medium enemy texture
	if (!mediumenemyTexture.initialize(graphics, "pictures\\mediumenemy.png"))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing mob texture"));

	//medium enemy image
	if (!mediumenemy.initialize(this, 130, 166, 6, &mediumenemyTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing mob"));
	mediumenemy.setFrames(mediumenemyNS::MediumEnemy_START_FRAME, mediumenemyNS::MediumEnemy_END_FRAME);
	mediumenemy.setFrameDelay(mediumenemyNS::ANIMATION_DELAY);
	mediumenemy.setX(mediumenemyNS::X);
	mediumenemy.setY(mediumenemyNS::Y);



	// bullet texture
	if (!bulletTexture.initialize(graphics, BULLET_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bullet textures"));

	//if (!heart1.initialize(this, heartNS::WIDTH, heartNS::HEIGHT, heartNS::TEXTURE_COLS, &easyenemyTexture))
		//throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing heart textures"));

	if (!heart1.initialize(this, heartNS::WIDTH , heartNS::HEIGHT, heartNS::TEXTURE_COLS, &heartTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing heart textures"));
	heart1.setX(30);
	heart1.setY(20);
	if (!heart2.initialize(this, heartNS::WIDTH, heartNS::HEIGHT, 1, &heartTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing heart textures"));
	heart2.setX(100);
	heart2.setY(20);
	if (!heart3.initialize(this, heartNS::WIDTH, heartNS::HEIGHT, 1, &heartTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing heart textures"));
	heart3.setX(170);
	heart3.setY(20);

	//heart2.setScale(0.1);
	//heart3.setScale(0.1);


	if (!tile.initialize(graphics, 0, 0, 0, &tileTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing tile textures"));

	if (!healthbar.initialize(graphics, 0, 0, 0, &healthbarTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing tile textures"));
	healthbar.setScale(0.1);
	healthbar.setX(30);
	healthbar.setY(20);

	if (!enemy.initialize(this, enemyNS::WIDTH, enemyNS::HEIGHT, 2, &enemyTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing planet"));
	enemy.setFrames(enemyNS::ENEMY_START_FRAME, enemyNS::ENEMY_END_FRAME);
	enemy.setCurrentFrame(enemyNS::ENEMY_START_FRAME);
	//enemy.setVelocity(VECTOR2(-enemyNS::SPEED, -enemyNS::)); // VECTOR2(X, Y)

    // ship2
    /*if (!ship2.initialize(this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &playerTextures))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player"));
    ship2.setFrames(shipNS::SHIP2_START_FRAME, shipNS::SHIP2_START_FRAME);
    ship2.setCurrentFrame(shipNS::SHIP2_START_FRAME);
	ship2.setFrameDelay(shipNS::ANIMATION_DELAY);
    ship2.setX(GAME_WIDTH - GAME_WIDTH/4);
    ship2.setY(GAME_HEIGHT/4);
    ship2.setVelocity(VECTOR2(-shipNS::SPEED,-shipNS::SPEED)); // VECTOR2(X, Y)
    return;*/

	/*if (input->isKeyDown(SPACE_KEY)) {
		if (!shadow.initialize(this, shadowNS::WIDTH, shadowNS::HEIGHT, shadowNS::TEXTURE_COLS, &shadowTextures))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player"));
		shadow.setFrames(shadowNS::SHADOW_START_FRAME, shadowNS::SHADOW_START_FRAME);
		shadow.setCurrentFrame(shadowNS::SHADOW_START_FRAME);
		shadow.setX(GAME_WIDTH - GAME_WIDTH / 4);
		shadow.setY(GAME_HEIGHT / 4);
		shadow.setVelocity(VECTOR2(-shadowNS::SPEED, -shadowNS::SPEED)); // VECTOR2(X, Y)
		return;
		shadow.draw();
	}*/
	
	/*if (!slash.initialize(this, slashNS::WIDTH, slashNS::HEIGHT, slashNS::TEXTURE_COLS, &slashTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player"));
	slash.setFrames(slashNS::SLASH_START_FRAME, slashNS::SLASH_END_FRAME);
	slash.setCurrentFrame(slashNS::SLASH_START_FRAME);
	slash.setFrameDelay(slashNS::ANIMATION_DELAY);
	slash.setX(GAME_WIDTH - GAME_WIDTH / 4);
	slash.setY(GAME_HEIGHT / 4);
	return;
	slash.draw();*/


	if (!shadow.initialize(this, shadowNS::WIDTH, shadowNS::HEIGHT, shadowNS::TEXTURE_COLS, &shadowStanding))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player"));
	shadow.setFrames(shadowNS::SHADOW_START_FRAME, shadowNS::SHADOW_END_FRAME);
	shadow.setCurrentFrame(shadowNS::SHADOW_START_FRAME);
	shadow.setFrameDelay(shadowNS::ANIMATION_DELAY);
	shadow.setX(GAME_WIDTH - GAME_WIDTH / 4);
	shadow.setY(GAME_HEIGHT / 4);
	shadow.setVelocity(VECTOR2(-shadowNS::SPEED, -shadowNS::SPEED)); // VECTOR2(X, Y)
	return;

	
}



//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{
	float playerX;
	float playerY;

	//hearts
	heart1.update(frameTime);
	heart2.update(frameTime);
	heart3.update(frameTime);
	slash.update(frameTime);
	ship1.update(frameTime);
	ship2.update(frameTime);
	bullet1.update(frameTime);
	bullet2.update(frameTime);
	shadow.update(frameTime);
	mediumenemy.update(frameTime);
	dmgTimer += frameTime;


	playerX = ship2.getX();
	playerY = ship2.getY();


	// side-scrolling (still doesn't work well
	if (playerX < 0) {
		mapX -= ship2.getVelocity().x * frameTime;
	}
	else if (playerX > GAME_WIDTH - ship2.getWidth()) {
		mapX -= ship2.getVelocity().x * frameTime;
		ship2.setX((float)GAME_WIDTH - ship2.getWidth());
	}

	if (mapX > 0)
	{
		mapX = 0;
		ship2.setVelocity(VECTOR2(shipNS::SPEED, 0));
	}
	else if (mapX < (-spacewarNS::MAP_WIDTH * spacewarNS::TEXTURE_SIZE) + GAME_WIDTH)
	{
		// stop at right edge of map
		mapX = (-spacewarNS::MAP_WIDTH * spacewarNS::TEXTURE_SIZE) + GAME_WIDTH;
		ship2.setVelocity(VECTOR2(0, 0));  // stop butterfly
	}

	// player input
	if (input->isKeyDown(SHIP2_UP_KEY))
	{
		ship2.setY(ship2.getY() - frameTime * shipNS::SPEED);
		//ship2.setFrames(shipNS::SHIP2_START_FRAME + 1, shipNS::SHIP2_END_FRAME);
	}
	if (input->isKeyDown(SHIP2_DOWN_KEY))
	{
		ship2.setY(ship2.getY() + frameTime * shipNS::SPEED);
		//ship2.setFrames(shipNS::SHIP2_START_FRAME + 1, shipNS::SHIP2_END_FRAME);
	}
	if (input->isKeyDown(SHIP2_LEFT_KEY))
	{
		ship2.setX(ship2.getX() - frameTime * shipNS::SPEED);
		//ship2.setFrames(shipNS::SHIP2_START_FRAME + 1, shipNS::SHIP2_END_FRAME);
		ship2.flipHorizontal(true);
	}
	if (input->isKeyDown(SHIP2_RIGHT_KEY))
	{
		ship2.setX(ship2.getX() + frameTime * shipNS::SPEED);
		//ship2.setFrames(shipNS::SHIP2_START_FRAME + 1, shipNS::SHIP2_END_FRAME);
		ship2.flipHorizontal(false);
	}

	// shadow input
	if (input->isKeyDown(SHIP2_UP_KEY))
	{
		shadow.setY(shadow.getY() - frameTime * shadowNS::SPEED);
		shadow.setFrames(shadowNS::SHADOW_WALK_START_FRAME, shadowNS::SHADOW_WALK_END_FRAME);
	}
	if (input->isKeyDown(SHIP2_DOWN_KEY))
	{
		shadow.setY(shadow.getY() + frameTime * shadowNS::SPEED);
		shadow.setFrames(shadowNS::SHADOW_WALK_START_FRAME, shadowNS::SHADOW_WALK_END_FRAME);
	}
	if (input->isKeyDown(SHIP2_LEFT_KEY))
	{
		shadow.setX(shadow.getX() - frameTime * shadowNS::SPEED);
		shadow.setFrames(shadowNS::SHADOW_WALK_START_FRAME, shadowNS::SHADOW_WALK_END_FRAME);
		shadow.flipHorizontal(false);
	}
	if (input->isKeyDown(SHIP2_RIGHT_KEY))
	{
		shadow.setX(shadow.getX() + frameTime * shadowNS::SPEED);
		shadow.setFrames(shadowNS::SHADOW_WALK_START_FRAME, shadowNS::SHADOW_WALK_END_FRAME);
		shadow.flipHorizontal(true);
	}
	if (input->isKeyDown(SPACE_KEY)) 
	{
		if (!slash.initialize(this, slashNS::WIDTH, slashNS::HEIGHT, slashNS::TEXTURE_COLS, &slashTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player"));
		shadow.setFrames(slashNS::SLASH_START_FRAME, slashNS::SLASH_END_FRAME);
		slash.setCurrentFrame(slashNS::SLASH_START_FRAME);
		slash.setFrameDelay(slashNS::ANIMATION_DELAY);
		slash.setX(shadow.getX());
		slash.setY(shadow.getY());
		slash.setVelocity(VECTOR2(-slashNS::SPEED, -slashNS::SPEED)); // VECTOR2(X, Y)
		return;


		/*if (!slash.initialize(this, slashNS::WIDTH, slashNS::HEIGHT, slashNS::TEXTURE_COLS, &slashTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player"));
		shadow.setFrames(slashNS::SLASH_START_FRAME, slashNS::SLASH_END_FRAME);
		shadow.setCurrentFrame(slashNS::SLASH_START_FRAME);
		shadow.setFrameDelay(slashNS::ANIMATION_DELAY);
		shadow.setVelocity(VECTOR2(-slashNS::SPEED, -slashNS::SPEED)); // VECTOR2(X, Y)
		return;*/
	}


	 

	//shooting
	if (input->isKeyDown(SHIP2_W_KEY)) {

		if (!bullet1.initialize(this, bulletNS::WIDTH, bulletNS::HEIGHT, bulletNS::TEXTURE_COLS, &bulletTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bullet"));

		bullet1.setX(ship2.getX());
		bullet1.setY(ship2.getY());


	}
	bullet1.setX(bullet1.getX() + 10);


	if (mediumenemy.getX() == ship2.getX())
	{
		if (!bullet2.initialize(this, bulletNS::WIDTH, bulletNS::HEIGHT, bulletNS::TEXTURE_COLS, &bulletTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bullet"));

		bullet2.setX(mediumenemy.getX());
		bullet2.setY(mediumenemy.getY());

	}
	bullet2.setX(bullet2.getX() + 10);
	//spanwer

	Timer += frameTime;
	if (Timer >= 3)
	{
		easyEnemy *e = new easyEnemy();

		//easy enemy image

		if (!e->initialize(this, 48, 48, 3, &easyenemyTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing mob"));
		e->setFrames(easyenemyNS::EasyEnemy_START_FRAME, easyenemyNS::EasyEnemy_END_FRAME);
		e->setFrameDelay(easyenemyNS::ANIMATION_DELAY);

		//	e->initialize(this, mushroom.WIDTH, mushroom.HEIGHT, mushroom.COLS, &mushroom.Texture);
		e->setX(rand() % GAME_WIDTH);
		e->setY(rand() % GAME_WIDTH);
		easyenemyList.push_back(e);

		Timer = 0;
	}



	for each (easyEnemy* e in easyenemyList)
	{
		//easyenemy movement
		float distanceX2 = abs(ship2.getX() - e->getX());
		float distanceY2 = abs(ship2.getY() - e->getY());
		float totaldistance2 = distanceX2 + distanceY2;
		float moveX2 = (distanceX2 / totaldistance2)*easyenemyNS::SPEED;
		float moveY2 = (distanceY2 / totaldistance2)*easyenemyNS::SPEED;
		if (ship2.getX() > e->getX())
		{
			e->setX(e->getX() + frameTime * easyenemyNS::SPEED);

		}
		if (ship2.getX() < e->getX())
		{
			e->setX(e->getX() - frameTime * easyenemyNS::SPEED);

		}
		if (ship2.getY() > e->getY())
		{
			e->setY(e->getY() + frameTime * easyenemyNS::SPEED);

		}
		if (ship2.getY() < e->getY())
		{
			e->setY(e->getY() - frameTime * easyenemyNS::SPEED);

		}
		e->update(frameTime); //framtime update
	}

	//special enemy movement
	float distanceX = abs(shadow.getX() - enemy.getX());
	float distanceY = abs(shadow.getY() - enemy.getY());
	float totaldistance = distanceX + distanceY;
	float moveX = (distanceX / totaldistance)*enemyNS::SPEED;
	float moveY = (distanceY / totaldistance)*enemyNS::SPEED;
	if (shadow.getX() > enemy.getX())
	{
		enemy.setX(enemy.getX() + frameTime * enemyNS::SPEED);

	}
	if (shadow.getX() < enemy.getX())
	{
		enemy.setX(enemy.getX() - frameTime * enemyNS::SPEED);

	}
	if (shadow.getY() > enemy.getY())
	{
		enemy.setY(enemy.getY() + frameTime * enemyNS::SPEED);

	}
	if (shadow.getY() < enemy.getY())
	{
		enemy.setY(enemy.getY() - frameTime * enemyNS::SPEED);

	}
	enemy.update(frameTime); //frametime udpdarte

	if (ship2.getHealth() == 0)
	{
		exitGame();
	}
}



	//=============================================================================
	// Artificial Intelligence
	//=============================================================================
void Spacewar::ai() {}


//=============================================================================
// Handle collisions
//=============================================================================
void Spacewar::collisions()
{
	VECTOR2 collisionVector;
	// if collision between ship and planet
	/*if (ship1.collidesWith(planet, collisionVector))
	{
		// bounce off planet
		ship1.bounce(collisionVector, planet);
		ship1.damage(PLANET);
	}
	if (ship2.collidesWith(planet, collisionVector))
	{
		// bounce off planet
		ship2.bounce(collisionVector, planet);
		ship2.damage(PLANET);
	}*/
	// if collision between ships

	for each (easyEnemy* e in easyenemyList)
	{
		if (e->collidesWith(ship2, collisionVector))
		{
			e->setVelocity(VECTOR2(-easyenemyNS::SPEED, -easyenemyNS::SPEED));
			if (shadow.getVunerability() && dmgTimer>3)
			{
				ship2.setHealth(ship2.getHealth() - easyenemyNS::DAMAGE);
				dmgTimer = 0;
			}
			if (ship2.getVunerability() && dmgTimer>3)
			{
				ship2.setHealth(ship2.getHealth() - easyenemyNS::DAMAGE);
				dmgTimer = 0;
			}
		}
	}

	/*	if (ship2.collidesWith(easyenemy, collisionVector))
		{
			ship2.bounce(collisionVector, easyenemy);
			ship2.damage(SHIP);
			//easyenemy.bounce(collisionVector*-1, ship2);
			//mob1.setVelocity = 0;
			//mob1.setY(mob1.getY());
			ship2.setVelocity(VECTOR2(-easyenemyNS::SPEED, -easyenemyNS::SPEED));
			//ship2.setHealth(ship2.getHealth() - easyenemyNS::DAMAGE);

		}*/
	
	
}

//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
    graphics->spriteBegin();                // begin drawing sprites

	for (int row = 0; row < spacewarNS::MAP_HEIGHT; row++)       // for each row of map
	{
		tile.setY((float)(row*spacewarNS::TEXTURE_SIZE)); // set tile Y
		for (int col = 0; col<spacewarNS::MAP_WIDTH; col++)    // for each column of map
		{
			if (spacewarNS::tileMap[row][col] >= 0)          // if tile present
			{
				//tile.setCurrentFrame(spacewarNS::tileMap[row][col]);    // set tile texture
				tile.setX((float)(col*spacewarNS::TEXTURE_SIZE));   // set tile X
																	// if tile on screen
				if (tile.getX() > -spacewarNS::TEXTURE_SIZE && tile.getX() < GAME_WIDTH)
					tile.draw();                // draw tile
			}
		}
	}
	
   // planet.draw();                          // add the planet to the scene
    ship1.draw();                           // add the spaceship to the scene
    ship2.draw();                           // add the spaceship to the scene
	enemy.draw();
	bullet1.draw();
	bullet2.draw();
	easyenemy.draw();
	for each (easyEnemy* e in easyenemyList)
	{
		e->draw();
	}
	heart1.draw();
	heart2.draw();
	heart3.draw();
	slash.draw();
	mediumenemy.draw();
	shadow.draw(); 
    graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{
    tileTexture.onLostDevice();
    gameTextures.onLostDevice();
    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{
    gameTextures.onResetDevice();
    tileTexture.onResetDevice();
    Game::resetAll();
    return;
}
