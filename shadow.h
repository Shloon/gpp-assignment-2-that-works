#ifndef _SHADOW_H                 // Prevent multiple definitions if this 
#define _SHADOW_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace shadowNS
{
	const int WIDTH = 331;                   // image width
	const int HEIGHT = 165;                  // image height
	const int X = GAME_WIDTH / 2 - WIDTH / 2;   // location on screen
	const int Y = GAME_HEIGHT / 2 - HEIGHT / 2;
	//const float ROTATION_RATE = (float)PI / 4; // radians per second
	const float SPEED = 100;                // 100 pixels per second
	const float MASS = 300.0f;              // mass
	const int   TEXTURE_COLS = 6;           // texture has 8 columns
	const int   SHADOW_WALK_START_FRAME = 3;      // ship1 starts at frame 0
	const int   SHADOW_WALK_END_FRAME = 5;        // ship1 animation frames 0,1,2,3
	const int   SHADOW_START_FRAME = 0;      // ship2 starts at frame 8
	const int   SHADOW_END_FRAME = 2;       // ship2 animation frames 8,9,10,11
	const float ANIMATION_DELAY = 0.2f;    // time between frames
	const int HEALTH = 1000000;
}

// inherits from Entity class
class Shadow : public Entity
{
private:
	bool    shieldOn;
	Image   shield;
	bool VUNERABLE = true;

public:



	// constructor
	Shadow();

	// inherited member functions
	virtual void draw();
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
		TextureManager *textureM);
	void update(float frameTime);
	void damage(WEAPON);
	virtual void setVunerability(bool i)
	{
		VUNERABLE = i;
	}
	virtual bool getVunerability() 
	{
		return VUNERABLE;
	}

};
	


#endif
