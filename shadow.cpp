#include "shadow.h"

//=============================================================================
// default constructor
//=============================================================================

Shadow::Shadow() : Entity()
{
	spriteData.width = shadowNS::WIDTH;           // size of Ship1
	spriteData.height = shadowNS::HEIGHT;
	spriteData.x = shadowNS::X;                   // location on screen
	spriteData.y = shadowNS::Y;
	spriteData.rect.bottom = shadowNS::HEIGHT;    // rectangle to select parts of an image
	spriteData.rect.right = shadowNS::WIDTH;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	startFrame = shadowNS::SHADOW_START_FRAME;     // first frame of ship animation
	endFrame = shadowNS::SHADOW_END_FRAME;     // last frame of ship animation
	currentFrame = startFrame;
	radius = shadowNS::WIDTH / 2.0;
	mass = shadowNS::MASS;
	collisionType = entityNS::CIRCLE;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Shadow::initialize(Game *gamePtr, int width, int height, int ncols,
	TextureManager *textureM)
{
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Shadow::draw()
{
	Image::draw();              
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Shadow::update(float frameTime)
{
	Entity::update(frameTime);
	//spriteData.angle += frameTime * shipNS::ROTATION_RATE;  // rotate the ship


	// Bounce off walls
	if (spriteData.x > GAME_WIDTH+120 - shadowNS::WIDTH)    // if hit right screen edge
	{
		spriteData.x = GAME_WIDTH+120 - shadowNS::WIDTH;    // position at right screen edge
		velocity.x = -velocity.x;                   // reverse X direction
	}
	else if (spriteData.x < -130)                    // else if hit left screen edge
	{
		spriteData.x = -130;                           // position at left screen edge
		velocity.x = -velocity.x;                   // reverse X direction
	}
	if (spriteData.y > GAME_HEIGHT - shadowNS::HEIGHT)  // if hit bottom screen edge
	{
		spriteData.y = GAME_HEIGHT - shadowNS::HEIGHT;  // position at bottom screen edge
		velocity.y = -velocity.y;                   // reverse Y direction
	}
	else if (spriteData.y < 0)                    // else if hit top screen edge
	{
		spriteData.y = 0;                           // position at top screen edge
		velocity.y = -velocity.y;                   // reverse Y direction
	}
}