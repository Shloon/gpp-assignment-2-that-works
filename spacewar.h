// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 spacewar.h v1.0

#ifndef _SPACEWAR_H             // Prevent multiple definitions if this 
#define _SPACEWAR_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "heart.h"
#include "ship.h"
#include "shadow.h"
#include "Enemy.h"
#include "bullet.h"
#include "mediumEnemy.h"
#include "easyEnemy.h"
#include "slash.h"

namespace spacewarNS
{
	
	const int MAP_WIDTH = 10;
	const int MAP_HEIGHT = 3;
	const int TEXTURE_SIZE = 250;
	const int __ = -1;

	
	
	const int tileMap[MAP_HEIGHT][MAP_WIDTH] = {
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	};
}

//=============================================================================
// This class is the core of the game
//=============================================================================
class Spacewar : public Game
{
private:
	//constants
	static const int MAX_BULLETS_NO = 10;
	//static const int MAX_BACKGROUND = 10;

	float mapX;
	float mapY;
	bool menuOn;

    // game items
    TextureManager tileTexture;   // nebula texture
    TextureManager gameTextures;    // game texture
	TextureManager playerTextures;
	TextureManager shadowTextures, shadowStanding;
	TextureManager enemyTexture;
	TextureManager  bulletTexture;
	TextureManager easyenemyTexture;
	TextureManager healthbarTexture;
	TextureManager heartTexture;
	TextureManager slashTexture;
	TextureManager mediumenemyTexture;
    Ship    ship1, ship2;           // spaceships
	Heart	heart1, heart2, heart3;        // the planet
    Image   tile;         // backdrop image
	Image	healthbar;
	//Image	backgroundList[MAX_BACKGROUND];
	Slash slash;
	Shadow shadow;
	Enemy enemy;			//enemy
	easyEnemy easyenemy;
	mediumEnemy mediumenemy;
	bullet bullet1, bullet2;
	bullet bulletList[MAX_BULLETS_NO];


	std::vector<easyEnemy*> easyenemyList;
	int enemyNo = 5;
	
	float Timer =0; 
	float dmgTimer = 0;

public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
};

#endif
