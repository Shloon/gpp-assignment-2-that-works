// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "ship.h"

//=============================================================================
// default constructor
//=============================================================================
Ship::Ship() : Entity()
{
    spriteData.width = shipNS::WIDTH;           // size of Ship1
    spriteData.height = shipNS::HEIGHT;
    spriteData.x = shipNS::X;                   // location on screen
    spriteData.y = shipNS::Y;
    spriteData.rect.bottom = shipNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = shipNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    startFrame = shipNS::SHIP2_START_FRAME;     // first frame of ship animation
    endFrame     = shipNS::SHIP2_END_FRAME;     // last frame of ship animation
	frameDelay = shipNS::ANIMATION_DELAY;
    currentFrame = startFrame;
    radius = shipNS::WIDTH/2.0;
    mass = shipNS::MASS;
    collisionType = entityNS::CIRCLE;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Ship::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Ship::draw()
{
    Image::draw();              // draw ship
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Ship::update(float frameTime)
{
    Entity::update(frameTime);
    //spriteData.angle += frameTime * shipNS::ROTATION_RATE;  // rotate the ship
    //spriteData.x += frameTime * velocity.x;         // move ship along X 
    //spriteData.y += frameTime * velocity.y;         // move ship along Y

    // Bounce off walls
    if (spriteData.x > GAME_WIDTH-shipNS::WIDTH)    // if hit right screen edge
    {
        spriteData.x = GAME_WIDTH-shipNS::WIDTH;    // position at right screen edge
        velocity.x = -velocity.x;                   // reverse X direction
    } else if (spriteData.x < 0)                    // else if hit left screen edge
    {
        spriteData.x = 0;                           // position at left screen edge
        velocity.x = -velocity.x;                   // reverse X direction
    }
    if (spriteData.y > GAME_HEIGHT-shipNS::HEIGHT)  // if hit bottom screen edge
    {
        spriteData.y = GAME_HEIGHT-shipNS::HEIGHT;  // position at bottom screen edge
        velocity.y = -velocity.y;                   // reverse Y direction
    } else if (spriteData.y < 0)                    // else if hit top screen edge
    {
        spriteData.y = 0;                           // position at top screen edge
        velocity.y = -velocity.y;                   // reverse Y direction
    }
}

//=============================================================================
// damage
//=============================================================================
void Ship::damage(WEAPON weapon)
{
    shieldOn = true;
}

/*void Ship::shootBullet(VECTOR2 player)
{
	bullet *a = new bullet(*);

	bulletList.push_back(a);
}


void Ship::drawBullet(float x, float y)
{
	for (std::vector<bullet*>::iterator it = bulletList.begin(); it != bulletList.end(); )
	{
		if ((*it)->getdel() == true) //if bullets have collided with player, del of bullet set to true
		{
			SAFE_DELETE(*it);
			it = bulletlist.erase(it);
		}
		else
		{
			++it;
		}
	}
	for each(bullet* p in bulletList)
	{
		p->setY(p-> y);
		p->setX(p->getMapX() + x);
		p->draw();

	}
}
*/