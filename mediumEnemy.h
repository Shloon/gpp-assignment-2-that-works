#ifndef _MEDIUMENEMY_H                 // Prevent multiple definitions if this 
#define _MEDIUMENEMY_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include"Enemy.h"
namespace mediumenemyNS
{

	const int WIDTH = 130;                   // image width
	const int HEIGHT = 166;                  // image height
	const int X = (rand() % GAME_WIDTH) - WIDTH / 2; //GAME_WIDTH / 2 - WIDTH / 2;   // location on screen
	const int Y = (rand() % GAME_HEIGHT) - HEIGHT / 2; //GAME_HEIGHT / 2 - HEIGHT / 2;
	const float SPEED = 50;                // 100 pixels per second
	const float MASS = 300.0f;              // mass
	const int   TEXTURE_COLS = 6;    // texture has 8 columns
	const int   MediumEnemy_START_FRAME = 0;      // ship1 starts at frame 0
	const int   MediumEnemy_END_FRAME = 5;        // 1 animation frames 0,1,2,3
	const float ANIMATION_DELAY = 0.3f;
	const int HEALTH = 10;
}

class mediumEnemy :public Enemy {

public:
	// constructor
	mediumEnemy();


};

#endif