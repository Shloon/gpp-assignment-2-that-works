// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _SHIP_H                 // Prevent multiple definitions if this 
#define _SHIP_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include <vector>
#include "bullet.h"

namespace shipNS
{
    const int WIDTH = 65;                   // image width
    const int HEIGHT = 61;                  // image height
    const int X = GAME_WIDTH/2 - WIDTH/2;   // location on screen
    const int Y = GAME_HEIGHT/2 - HEIGHT/2;
    //const float ROTATION_RATE = (float)PI/4; // radians per second
    const float SPEED = 100;                // 100 pixels per second
    const float MASS = 300.0f;              // mass
    const int   TEXTURE_COLS = 9;           // texture has 8 columns
    const int   SHIP2_START_FRAME = 0;      // ship2 starts at frame 8
    const int   SHIP2_END_FRAME = 8;     // ship2 animation frames 8,9,10,11
	const float ANIMATION_DELAY = 0.1f;
	const int HEALTH = 1500;
}

// inherits from Entity class
class Ship : public Entity
{
private:
    bool    shieldOn;
	std::vector<bullet*> bulletList;
	bool VUNERABLE = true;

public:
    // constructor
    Ship();

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
    void damage(WEAPON);

	void Ship::shootBullet(VECTOR2 enemy);
	void Ship::drawBullet(float x, float y);
	virtual void setVunerability(bool i)
	{
		VUNERABLE = i;
	}
	virtual bool getVunerability()
	{
		return VUNERABLE;
	}
};
#endif

