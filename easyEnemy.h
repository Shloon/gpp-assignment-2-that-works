#ifndef _EASYENEMY_H                 // Prevent multiple definitions if this 
#define _EASYENEMY_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include"Enemy.h"
namespace easyenemyNS
{

	const int WIDTH = 32;                   // image width
	const int HEIGHT = 32;                  // image height
	const int X = (rand() % GAME_WIDTH) - WIDTH / 2; //GAME_WIDTH / 2 - WIDTH / 2;   // location on screen
	const int Y = (rand() % GAME_HEIGHT) - HEIGHT / 2; //GAME_HEIGHT / 2 - HEIGHT / 2;
	const float SPEED = 50;                // 100 pixels per second
	const float MASS = 300.0f;              // mass
	const int   TEXTURE_COLS = 3;    // texture has 8 columns
	const int   EasyEnemy_START_FRAME = 3;      // ship1 starts at frame 0
	const int   EasyEnemy_END_FRAME = 5;        // 1 animation frames 0,1,2,3
	const float ANIMATION_DELAY = 10.5f;
	const int HEALTH = 10;
	const float DAMAGE = 0.5;
}

class easyEnemy :public Enemy {

public:
	// constructor
	easyEnemy();


};

#endif