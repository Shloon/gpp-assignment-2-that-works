#ifndef _BULLET_H
#define _BULLET_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace bulletNS
{
	const int WIDTH = 10;                   // image width
	const int HEIGHT = 10;                  // image height
	const int X = GAME_WIDTH / 3;   // location on screen
	const int Y = GAME_HEIGHT / 2;
	//const float ROTATION_RATE = (float)PI/4; // radians per second
	const float SPEED = 10;                // 100 pixels per second
	const float MASS = 300.0f;              // mass
	const int   TEXTURE_COLS = 1;           // texture has 8 columns
	const int   BULLET_START_FRAME = 1;      // ship2 starts at frame 8
	const int   BULLET_END_FRAME = 1;     // bullet frames
	const int DAMAGE = 1;
}

// inherits from Entity class
class bullet : public Entity
{
private:
	bool    shieldOn;

public:
	// constructor
	bullet();

	// inherited member functions
	virtual void draw();
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
		TextureManager *textureM);
	void update(float frameTime);
	void damage(WEAPON);
	//virtual bool getdel();
};

#endif
