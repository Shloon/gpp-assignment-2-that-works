#ifndef _SLASH_H                 // Prevent multiple definitions if this 
#define _SLASH_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include"shadow.h"


namespace slashNS
{
	const int WIDTH = 331;                   // image width
	const int HEIGHT = 166;                  // image height
	const int X = GAME_WIDTH / 2 - WIDTH / 2;   // location on screen
	const int Y = GAME_HEIGHT / 2 - HEIGHT / 2;
	const float SPEED = 100;                // 100 pixels per second
	const float MASS = 300.0f;              // mass
	const int   TEXTURE_COLS = 9;           // texture has 8 columns
	const int   SLASH_START_FRAME = 0;      // ship1 starts at frame 0
	const int   SLASH_END_FRAME = 8;        // ship1 animation frames 0,1,2,3
	const float ANIMATION_DELAY = 0.1f;    // time between frames
	const int DAMAGE = 10;
}


class Slash :public Shadow {

public:
	// constructor
	Slash();


};
#endif
