// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 version 1.0

#include "heart.h"

//=============================================================================
// default constructor
//=============================================================================
Heart::Heart() : Entity()
{
    spriteData.x    = heartNS::X;              // location on screen
    spriteData.y    = heartNS::Y;
    radius          = heartNS::COLLISION_RADIUS;
    mass            = heartNS::MASS;
    startFrame      = heartNS::START_FRAME;    // first frame of ship animation
    endFrame        = heartNS::END_FRAME;      // last frame of ship animation
    setCurrentFrame(startFrame);
}